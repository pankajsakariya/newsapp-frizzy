package com.newsapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.newsapp.NewsFeedAdapter.NewsFeedBaseAdapter;
import com.newsapp.NewsFeedView.NewsFeedListView;
import com.newsapp.NewsFeedModel.NewsFeedModel;
import com.newsapp.NewsFeedService.NewsFeedService;
import com.newsapp.NewsFeedUtility.NewsFeedUtility;

import java.util.List;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<NewsFeedModel>>, SwipeRefreshLayout.OnRefreshListener {

    private static final int LOADER_ID = 0;

    NewsFeedBaseAdapter baseAdapter;

    SwipeRefreshLayout swipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipe = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipe.setOnRefreshListener(this);
        swipe.setColorSchemeColors(getResources().getColor(R.color.colorAccent));

        NewsFeedListView listview = (NewsFeedListView) findViewById(R.id.listview);

        baseAdapter = new NewsFeedBaseAdapter(MainActivity.this);

        listview.setAdapter(baseAdapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent contentBrowser = new Intent("android.intent.action.VIEW", Uri.parse(baseAdapter.getItem(position).getWebUrl()));
                startActivity(contentBrowser);
            }
        });

        if (NewsFeedUtility.isNetworkAvailable(MainActivity.this)) {
            getSupportLoaderManager().initLoader(LOADER_ID, null, this);
        } else {
            Toast.makeText(MainActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRefresh() {
        if (NewsFeedUtility.isNetworkAvailable(MainActivity.this)) {
            getSupportLoaderManager().restartLoader(LOADER_ID, null, this);
        } else {
            Toast.makeText(MainActivity.this, getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            swipe.setRefreshing(false);
        }
    }

    @Override
    public Loader<List<NewsFeedModel>> onCreateLoader(int id, Bundle args) {
        return new NewsFeedService(this);
    }

    @Override
    public void onLoadFinished(Loader<List<NewsFeedModel>> loader, List<NewsFeedModel> data) {
        swipe.setRefreshing(false);
        if (null != data) {
            baseAdapter.clear();
            baseAdapter.addFeeds(data);
            baseAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(Loader<List<NewsFeedModel>> loader) {

    }
}
