package com.newsapp.NewsFeedAdapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.newsapp.R;
import com.newsapp.NewsFeedView.NewsFeedTextView;
import com.newsapp.NewsFeedModel.NewsFeedModel;

import java.util.ArrayList;
import java.util.List;


public class NewsFeedBaseAdapter extends BaseAdapter {

    private Context context;

    private List<NewsFeedModel> newsFeedModels;

    public NewsFeedBaseAdapter(Context context) {
        this.context = context;
        this.newsFeedModels = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return newsFeedModels.size();
    }

    @Override
    public NewsFeedModel getItem(int position) {
        return newsFeedModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Typeface fonts1 = Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Light.ttf");

        Typeface fonts2 = Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Regular.ttf");

        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list, null);

            viewHolder = new ViewHolder();

            viewHolder.title = (NewsFeedTextView) convertView.findViewById(R.id.title);
            viewHolder.publicationDate = (NewsFeedTextView) convertView.findViewById(R.id.publicationDate);
            viewHolder.sectioName = (NewsFeedTextView) convertView.findViewById(R.id.sectionName);
            viewHolder.webUrl = (NewsFeedTextView) convertView.findViewById(R.id.weburl);

            viewHolder.title.setTypeface(fonts1);
            viewHolder.publicationDate.setTypeface(fonts1);

            viewHolder.title.setTypeface(fonts2);
            viewHolder.sectioName.setTypeface(fonts2);

            convertView.setTag(viewHolder);
        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        NewsFeedModel item = getItem(position);

        viewHolder.title.setText(item.getTitle());
        viewHolder.sectioName.setText(item.getSectioName());
        viewHolder.publicationDate.setText(item.getPublicationDate());
        viewHolder.webUrl.setText(item.getWebUrl());

        return convertView;
    }

    public void addFeeds(List<NewsFeedModel> feedArrayList) {
        this.newsFeedModels = feedArrayList;
    }

    public void clear() {
        newsFeedModels.clear();
    }

    private class ViewHolder {

        NewsFeedTextView sectioName;
        NewsFeedTextView publicationDate;
        NewsFeedTextView title;
        NewsFeedTextView webUrl;
    }
}

